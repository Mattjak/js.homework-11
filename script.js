
const btn = document.getElementsByClassName('btn');
const ArrayOfButtons = Array.prototype.slice.call(btn);
document.addEventListener('keypress', function (event) {
    const key = event.key;
    let listOfKeys = ArrayOfButtons.find(event => event.innerHTML.toUpperCase() === key.toUpperCase());
    if (listOfKeys) {
        for (let i = 0; i < ArrayOfButtons.length; i++) {
            ArrayOfButtons[i].classList.remove('active');
        }
        listOfKeys.classList.add('active');
    }
});
